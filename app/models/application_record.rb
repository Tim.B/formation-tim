# frozen_string_literal: true

# Set common config/methods for all models
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
