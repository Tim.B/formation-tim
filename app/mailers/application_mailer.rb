# frozen_string_literal: true

# Set common config/methods for all Mailers
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
